from typing import Optional, List, Callable
import logging
import threading
import time
from kortex_api.TCPTransport import TCPTransport
from kortex_api.RouterClient import RouterClient, RouterClientSendOptions
from kortex_api.SessionManager import SessionManager
from kortex_api.autogen.messages import Session_pb2
from kortex_api.autogen.client_stubs.BaseClientRpc import BaseClient
from kortex_api.autogen.client_stubs.BaseCyclicClientRpc import BaseCyclicClient
from kortex_api.autogen.messages import Base_pb2, BaseCyclic_pb2, Common_pb2

logger = logging.getLogger(__name__)


class KinovaRobotError(Exception):
    pass


class KinovaRobotCannotReachHomePositionError(KinovaRobotError):
    pass


class KinovaRobotActionTimeoutError(KinovaRobotError):
    pass


class KinovaRobotActionAbortError(KinovaRobotError):
    pass


class KinovaRobotBusyError(KinovaRobotError):
    pass


class KinovaRobotActionNotFoundError(KinovaRobotError):
    pass


class KinovaRobotInvalidJointsLengthError(KinovaRobotError):
    pass


class KinovaRobot:
    def __init__(self, host='129.168.1.10', port='10000', username='admin', password='admin'):
        self.host = host
        self.port = port
        self.username = username
        self.password = password
        self.notification_callbacks: List[Callable] = []
        self.connected = False
        self.busy = False
        self._actuator_count = None

        self.logger = logger.getChild(self.__class__.__name__)
        self.transport = TCPTransport()
        self.router = RouterClient(self.transport)
        self.session_manager = SessionManager(self.router)
        self.client: Optional[BaseClient] = None
        self.cyclic_client: Optional[BaseCyclicClient] = None

    @property
    def actuator_count(self) -> int:
        if self._actuator_count is not None:
            return self._actuator_count

        self._actuator_count = self.client.GetActuatorCount().count
        return self._actuator_count

    def connect(self):
        if self.connected:
            return

        session = Session_pb2.CreateSessionInfo()
        session.username = self.username
        session.password = self.password
        session.session_inactivity_timeout = 10000
        session.connection_inactivity_timeout = 2000

        self.transport.connect(self.host, self.port)
        self.session_manager.CreateSession(session)
        self.client = BaseClient(self.router)
        self.cyclic_client = BaseCyclicClient(self.router)

        self.subcribe()
        self.set_servo_mode(Base_pb2.SINGLE_LEVEL_SERVOING)

        self.connected = True

    def disconnect(self):
        self.unsubscribe()

    def subcribe(self):
        self.subscription = self.client.OnNotificationActionTopic(self.on_notification, Base_pb2.NotificationOptions())

    def unsubscribe(self):
        self.client.Unsubscribe(self.subscription)

    def on_notification(self, notification: Base_pb2.ActionNotification):
        action_event = notification.action_event

        if action_event == Base_pb2.ACTION_START:
            self.busy = True
        elif action_event == Base_pb2.ACTION_END:
            self.busy = False
        elif action_event == Base_pb2.ACTION_ABORT:
            self.busy = False
            self.logger.warning(f'Action aborted')
        elif action_event == Base_pb2.ACTION_PAUSE:
            self.busy = False

    def wait_for_ready(self):
        while self.busy:
            time.sleep(0.01)

    def execute_existing_action(self, name: str, action_type: int, wait: bool = True):
        if self.busy:
            raise KinovaRobotBusyError(f'Cannot execute existing action "{name}", robot busy')

        requested_action_type = Base_pb2.RequestedActionType()
        requested_action_type.action_type = action_type
        action_list = self.client.ReadAllActions(requested_action_type)
        action_handle = None

        for action in action_list:
            if action.name == name:
                action_handle = action_handle

        if action_handle is None:
            raise KinovaRobotActionNotFoundError(f'Action not found: {name}')

        self.client.ExecuteActionFromReference(action_handle)

        if wait:
            self.wait_for_ready()

    def execute_action(self, action, wait: bool = True):
        if self.busy:
            raise KinovaRobotBusyError(f'Cannot execute existing action ({action}), robot busy')

        self.client.ExecuteAction(action)

        if wait:
            self.wait_for_ready()

    def set_servo_mode(self, mode: int):
        servo_mode = Base_pb2.ServoingModeInformation()
        servo_mode.servoing_mode = mode
        self.client.SetServoingMode(servo_mode)

    def home(self, wait: bool = True):
        try:
            self.execute_existing_action('Home', Base_pb2.REACH_JOINT_ANGLES, wait=wait)
        except KinovaRobotActionNotFoundError:
            raise KinovaRobotCannotReachHomePositionError(f'Cannot reach home position')

    def move_to(self, x=None, y=None, z=None, rx=None, ry=None, rz=None, relative=False, wait: bool = True):
        feedback = self.cyclic_client.RefreshFeedback()
        action = Base_pb2.Action()
        action.action_type = Base_pb2.REACH_POSE

        target_pose = action.reach_pose.target_pose
        current_pose = feedback.base

        if not relative:
            target_pose.x = x if x is not None else current_pose.tool_pose_x
            target_pose.y = y if y is not None else current_pose.tool_pose_y
            target_pose.z = z if z is not None else current_pose.tool_pose_z
            target_pose.theta_x = rx if rx is not None else current_pose.tool_pose_theta_x
            target_pose.theta_y = ry if ry is not None else current_pose.tool_pose_theta_y
            target_pose.theta_z = rz if rz is not None else current_pose.tool_pose_theta_z
        else:
            target_pose.x = current_pose.tool_pose_x + (x or 0)
            target_pose.y = current_pose.tool_pose_y + (y or 0)
            target_pose.z = current_pose.tool_pose_z + (z or 0)
            target_pose.theta_x = current_pose.tool_pose_theta_x + (rx or 0)
            target_pose.theta_y = current_pose.tool_pose_theta_y + (ry or 0)
            target_pose.theta_z = current_pose.tool_pose_theta_z + (rz or 0)

        self.execute_action(action, wait=wait)

    def move_joints(self, joint_angles: List[float], wait: bool = True):
        if len(joint_angles) != self.actuator_count:
            raise KinovaRobotInvalidJointsLengthError(f'Invalid number of joint angles ({len(joint_angles)}), must be {self.client.GetActuatorCount()}')

        action = Base_pb2.Action()
        action.action_type = Base_pb2.REACH_JOINT_ANGLES

        for joint_id in range(self.actuator_count):
            joint_angle = action.reach_joint_angles.joint_angles.joint_angles.add()
            joint_angle.joint_identifier = joint_id
            joint_angle.value = joint_angles[joint_id]

        self.execute_action(action, wait=wait)

    def close_gripper(self, position: float=1.0):
        command = Base_pb2.GripperCommand()
        command.mode = Base_pb2.GRIPPER_POSITION
        finger = command.gripper.finger.add()
        finger.finger_identifier = 1
        finger.value = position
        self.client.SendGripperCommand(command)
        self.wait_for_ready()

    def open_gripper(self, position: float = 0.0):
        self.close_gripper(position)

    def stop(self):
        self.client.Stop()
